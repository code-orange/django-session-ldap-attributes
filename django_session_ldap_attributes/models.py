from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from jsonfield import JSONField


class UserLdapAttributes(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    json_data = JSONField(null=True)
    user_thumbnail = models.BinaryField(null=True)

    class Meta:
        db_table = "auth_user_ldap_attributes"


@receiver(post_save, sender=User)
def create_user_ldap_attributes(sender, instance, created, **kwargs):
    try:
        ldap_attributes = UserLdapAttributes.objects.get(user=instance)
    except UserLdapAttributes.DoesNotExist:
        UserLdapAttributes.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_ldap_attributes(sender, instance, **kwargs):
    instance.userldapattributes.save()
