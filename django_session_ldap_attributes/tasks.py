from celery import shared_task
from django.core.management import call_command


@shared_task(name="ldap_sync_users")
def ldap_sync_users():
    call_command("ldap_sync_users")

    return
