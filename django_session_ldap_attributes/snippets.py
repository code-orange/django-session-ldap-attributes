from django_mdat_customer.django_mdat_customer.models import MdatCustomers


def get_mdat_customer_for_user(user):
    org_tag = user.userldapattributes.json_data["extensionAttribute1"][0]
    return MdatCustomers.objects.get(org_tag=org_tag)
