from functools import wraps

from django.shortcuts import redirect
from django.urls import NoReverseMatch

from django_session_ldap_attributes.django_session_ldap_attributes.permissions import (
    user_group_check,
    user_admin_group_check,
)


def group_required(group_name: str):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if request.user.is_authenticated:
                if user_group_check(request.user, group_name):
                    return view_func(request, *args, **kwargs)

            try:
                return redirect("main-access-denied")
            except NoReverseMatch:
                return redirect("login")

        return _wrapped_view

    return decorator


def admin_group_required(group_name: str):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if request.user.is_authenticated:
                if user_admin_group_check(request.user, group_name):
                    return view_func(request, *args, **kwargs)

            try:
                return redirect("main-access-denied")
            except NoReverseMatch:
                return redirect("login")

        return _wrapped_view

    return decorator
