import json


def sync_user_relations(user, ldap_attributes):
    user.userldapattributes.json_data = json.loads(
        json.dumps(dict(ldap_attributes), default=lambda o: "<not serializable>")
    )

    if "thumbnailPhoto" in ldap_attributes:
        user.userldapattributes.user_thumbnail = ldap_attributes["thumbnailPhoto"][0]

    user.userldapattributes.save()
