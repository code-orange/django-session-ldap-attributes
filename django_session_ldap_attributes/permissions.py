from django.conf import settings

from django_session_ldap_attributes.django_session_ldap_attributes.snippets import (
    get_mdat_customer_for_user,
)


def user_group_check(user, group_name):
    org_tag = get_mdat_customer_for_user(user).org_tag

    check_group_dn = "CN={},OU={},{}".format(
        group_name,
        org_tag,
        settings.LDAP_AUTH_SEARCH_BASE,
    )

    for group_dn in user.userldapattributes.json_data["memberOf"]:
        if group_dn.lower() == check_group_dn.lower():
            return True

    return False


def user_admin_group_check(user, group_name):
    org_tag = get_mdat_customer_for_user(user).org_tag

    if user_group_check(
        user,
        "Admins@{}".format(
            org_tag,
        ),
    ):
        return True

    return user_group_check(
        user,
        "{}_Admins@{}".format(
            group_name,
            org_tag,
        ),
    )
